FindRuby-compat-vars
--------------------

* The :module:`FindRuby` module no longer provides variables with the
  upper-case ``RUBY_`` prefix.  See policy :policy:`CMP0185`.
