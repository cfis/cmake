cpack-tar
---------

* The :cpack_gen:`CPack Archive Generator` learned to generated `.tar`
  packages without compression.
